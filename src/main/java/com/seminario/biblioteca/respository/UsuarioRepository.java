/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.Usuario;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("usuarioRepository")
public interface UsuarioRepository extends JpaRepository<Usuario, Object> {

    @Query(value = "select * from usuario where persona = :idPersona",
    nativeQuery = true)
    Usuario findUsuarioByPersona(@Param("idPersona") int idPersona) throws SQLException, DataAccessException;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into usuario (codigo, password, persona) " +
            "values(:codigo, :password, :persona)",
    nativeQuery = true)
    int createUsuarioByPersona(@Param("codigo") String codigo,
                      @Param("password") String password,
                      @Param("persona") Integer persona) throws SQLException, DataAccessException;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update usuario set password =:password where persona =:persona",
    nativeQuery = true)
    int updateUsuarioByPersona(@Param("password") String password,
                               @Param("persona") Integer persona) throws SQLException, DataAccessException;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "delete from usuario where persona =:persona",
    nativeQuery = true)
    int deleteUsuarioByPersona(@Param("persona") Integer persona) throws SQLException, DataAccessException;

}
