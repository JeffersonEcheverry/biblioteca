/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.Persona;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("personaRepository")
public interface PersonaRepository extends JpaRepository<Persona, Object>{
    
    @Query(value = "select * from persona where documento =:documento",
            nativeQuery = true)
    public Persona findPersonaByDocumento(@Param("documento") String documento) throws SQLException, DataAccessException;
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into persona (apellido, documento, nombre, telefono, tipo_usuario) "
            + "values(:apellido, :documento, :nombre, :telefono, :tipoUsuario)",
            nativeQuery = true)
    public int savePersona(@Param("apellido") String apellido,
                           @Param("documento") String documento,
                           @Param("nombre") String nombre,
                           @Param("telefono") String telefono,
                           @Param("tipoUsuario") Integer tipoUsuario) throws SQLException, DataAccessException;
    
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "update persona set nombre =:nombre, telefono =:telefono, tipo_usuario =:tipoUsuario "
            + "where documento =:documento",
            nativeQuery = true)
    public int updatePersona(@Param("nombre") String nombre,
                             @Param("telefono") String telefono,
                             @Param("tipoUsuario") Integer tipoUsuario,
                             @Param("documento") String documento) throws SQLException, DataAccessException;
    
    @Query(value = "delete from persona where documento =:documento",
            nativeQuery = true)
    public int deletePersona(@Param("documento") String documento) throws SQLException, DataAccessException;
}
