package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.Libro;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;

@Repository("libroRepository")
public interface LibroRepository extends JpaRepository<Libro, Object> {

    @Query(value = "select * from libro where id =:id",
    nativeQuery = true)
    Libro findLibroById(@Param("id") Integer id) throws SQLException, DataAccessException;

    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "insert into (autor, nombre_libro, genero, descripcion, ubicacion) " +
            "values(:autor, :nombreLibro, :genero, :descripcion, :ubicacion)",
    nativeQuery = true)
    int saveLibro(@Param("autor") String autor,
                  @Param("nombreLibro") String nombreLibro,
                  @Param("genero") String genero,
                  @Param("descripcion") String descripcion,
                  @Param("ubicacion") Integer ubicacion) throws SQLException, DataAccessException;

    @Query(value = "update libro set autor =:autor, nombre_libro =:nombreLibro, genero =:genero, descripcion =:descripcion," +
            "",
    nativeQuery = true)
    int updateLibro() throws SQLException, DataAccessException;

}
