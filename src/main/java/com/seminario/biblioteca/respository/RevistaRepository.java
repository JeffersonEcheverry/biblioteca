package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.Revista;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("revistaRepository")
public interface RevistaRepository extends JpaRepository<Revista, Object> {

}
