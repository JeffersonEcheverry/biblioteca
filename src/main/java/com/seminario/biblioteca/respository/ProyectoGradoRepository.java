package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.ProyectoGrado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("proyectoGradoRepository")
public interface ProyectoGradoRepository extends JpaRepository<ProyectoGrado, Object> {

}
