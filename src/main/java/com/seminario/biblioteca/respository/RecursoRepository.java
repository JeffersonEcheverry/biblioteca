/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.respository;

import com.seminario.biblioteca.entity.Recurso;
import java.sql.SQLException;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jeffersonecheverry
 */
@Repository("recursoRepository")
public interface RecursoRepository extends JpaRepository<Recurso, Object>{
    
    @Query(value = "select * from recursos",
            nativeQuery = true)
    public Recurso findAllRecurso() throws SQLException, DataAccessException;
    
}
