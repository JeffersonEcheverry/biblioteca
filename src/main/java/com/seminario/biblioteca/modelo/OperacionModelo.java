/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.modelo;

import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Data
public class OperacionModelo {
    
    private String operacion;
    
    private String body;
    
}
