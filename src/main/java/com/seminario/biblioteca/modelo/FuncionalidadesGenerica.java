package com.seminario.biblioteca.modelo;

import com.google.gson.Gson;

public class FuncionalidadesGenerica {

    private static Gson gson;

    public static String createOperacionModelo(String operacion, String body){
        OperacionModelo operacionModelo = new OperacionModelo();
        operacionModelo.setOperacion(operacion);
        operacionModelo.setBody(body);
        return gson.toJson(operacionModelo);
    }
}
