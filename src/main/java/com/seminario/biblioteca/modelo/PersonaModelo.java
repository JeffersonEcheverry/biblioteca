/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.modelo;

import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Data
public class PersonaModelo {
    
    private String nombre;
    private String apellido;
    private String documento;
    private String telefono;
    private Integer tipoUsuario;
    private String codigo;
    private String password;
    
}
