/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.modelo;

/**
 *
 * @author jeffersonecheverry
 */
public enum AuthorityName {
    
    ROLE_VINCULADO, ROLE_ADMIN, ROLE_NO_VINCULADO
    
}
