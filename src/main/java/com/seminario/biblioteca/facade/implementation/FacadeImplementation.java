/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.facade.implementation;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.seminario.biblioteca.facade.Facade;
import com.seminario.biblioteca.modelo.PersonaModelo;
import com.seminario.biblioteca.modelo.ResponseModelo;
import com.seminario.biblioteca.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.function.EntityResponse;

/**
 *
 * @author jeffersonecheverry
 */
@Component("facade")
public class FacadeImplementation implements Facade {
    
    private Gson gson;
    
    @Autowired
    @Qualifier("personaService")
    private PersonaService personaService;

    @Value("${EXITO}")
    private int codigoExito;

    @Value("${MENSAJE_EXITO}")
    private String mensajeExito;

    @Value("${FALLIDO}")
    private int codigoFallo;

    @Value("${MENSAJE_FALLIDO}")
    private String mensajeFallido;

    @Value("${PERSONA_NO_ENCONTRADA}")
    private String personaNoEncontrada;

    @Value("${PERSONA_NO_GUARDADA}")
    private String personaNoGuardada;

    @Value("${PERSONA_NO_DELETE}")
    private String personaNoEliminada;

    @Override
    public String operation(String json) {
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
        String operacion = jsonObject.get("operacion").getAsString() != null ? jsonObject.get("operacion").getAsString() : "";
        JsonObject body = null;
        try{
             body = jsonObject.get("body").getAsJsonObject() != null ? jsonObject.get("body").getAsJsonObject() : null;
        }catch(Exception e){
            e.printStackTrace();
        }

        switch (operacion) {
            case "BUSCARPERSONA":{
                String documento = jsonObject.get("body").getAsString() != null ? jsonObject.get("body").getAsString() : "";
                ResponseModelo responseModelo;
                try{
                    PersonaModelo personaModelo = personaService.findPersona(documento);
                    if(personaModelo != null){
                        responseModelo = createResponse(codigoExito, mensajeExito);
                        responseModelo.setBody(personaModelo);
                    } else {
                        responseModelo = createResponse(codigoFallo, personaNoEncontrada);
                        responseModelo.setBody(null);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    responseModelo = createResponse(codigoFallo, e.getMessage());
                    responseModelo.setBody(null);
                }
                return gson.toJson(responseModelo);
            }
            case "CREARPERSONA": {
                PersonaModelo personaModelo = new PersonaModelo();
                ResponseModelo responseModelo = new ResponseModelo();
                if (body != null) {
                    personaModelo.setNombre(body.get("nombre").getAsString() != null ? body.get("nombre").getAsString() : "");
                    personaModelo.setApellido(body.get("apellido").getAsString() != null ? body.get("apellido").getAsString() : "");
                    personaModelo.setDocumento(body.get("documento").getAsString() != null ? body.get("documento").getAsString() : "");
                    personaModelo.setTelefono(body.get("telefono").getAsString() != null ? body.get("telefono").getAsString() : "");
                    personaModelo.setTipoUsuario(body.get("tipoUsuario").getAsInt() > 0 ? body.get("tipoUsuario").getAsInt() : null);
                    personaModelo.setCodigo(body.get("codigo").getAsString() != null ? body.get("codigo").getAsString() : "");
                    personaModelo.setPassword(body.get("password").getAsString() != null ? body.get("password").getAsString() : "");
                    try {
                        PersonaModelo personaModeloResponse = personaService.savePersona(personaModelo);
                        if(personaModeloResponse != null){
                            responseModelo = createResponse(codigoExito, mensajeExito);
                            responseModelo.setBody(personaModeloResponse);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        responseModelo = createResponse(codigoFallo, e.getMessage());
                        responseModelo.setBody(null);
                    }
                } else {
                    responseModelo = createResponse(codigoFallo, personaNoGuardada);
                    responseModelo.setBody(null);
                }
                return  gson.toJson(responseModelo);
            }
            case "DELETEPERSONA": {
                String documento = jsonObject.get("body").getAsString() != null ? jsonObject.get("body").getAsString() : "";
                ResponseModelo responseModelo;
                try{
                    if(personaService.deletePersona(documento)){
                        responseModelo = createResponse(codigoExito, mensajeExito);
                        responseModelo.setBody(true);
                    } else {
                        responseModelo = createResponse(codigoFallo, personaNoEliminada);
                        responseModelo.setBody(false);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                    responseModelo = createResponse(codigoFallo, e.getMessage());
                    responseModelo.setBody(false);
                }
                return gson.toJson(responseModelo);
            }
            default: {

            }
        }

        return null;
    }

    private ResponseModelo createResponse(int codigoRetorno, String mensajeRetorno){

        ResponseModelo responseModelo = new ResponseModelo();
        responseModelo.setCodigoRetorno(codigoRetorno);
        responseModelo.setMensaje(mensajeRetorno);

        return responseModelo;
    }

}
