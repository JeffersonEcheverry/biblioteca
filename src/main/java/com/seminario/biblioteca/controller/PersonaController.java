/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.controller;

import com.google.gson.Gson;
import com.seminario.biblioteca.facade.Facade;
import com.seminario.biblioteca.modelo.FuncionalidadesGenerica;
import com.seminario.biblioteca.modelo.OperacionModelo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeffersonecheverry
 */
@RestController
@RequestMapping("/persona")
public class PersonaController {
    
    private Gson gson;
    
    @Autowired
    @Qualifier("facade")
    private Facade facade;
    
    @Value("${BUSCAR_PERSONA}")
    private String buscar;
    
    @Value("${DELETE_PERSONA}")
    private String delete;
    
    @GetMapping("/findPersona/{documento}")
    public @ResponseBody String buscarPersona(@PathVariable String documento){
        return facade.operation(FuncionalidadesGenerica.createOperacionModelo(buscar, documento));
    }
    
    @PostMapping("/savePersona")
    public @ResponseBody String guardarPersona(@RequestBody String json){
        return facade.operation(json);
    }
    
    @DeleteMapping("/deletePersona/{documento}")
    public @ResponseBody String eliminarPersona(@PathVariable String documento){
        return facade.operation(FuncionalidadesGenerica.createOperacionModelo(delete, documento));
    }

}
