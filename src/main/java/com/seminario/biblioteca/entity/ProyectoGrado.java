/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity(name = "ProyectoGrado")
@Table(name = "proyecto_grado")
@Data
public class ProyectoGrado implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "nombreProyecto", nullable = false)
    private String nombreProyecto;
    
    @Column(name = "fechaProyecto", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha; 
    
    @Column(name = "numeroPaginas", nullable = false)
    private Integer numeroPaginas;
    
    @Column(name = "descripcion", nullable = false)
    private String descripcion;
    
    @OneToMany(mappedBy = "proyectoGrado")
    private List<Recurso> recursos;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ubicacion", referencedColumnName = "id", nullable = false)
    private Ubicacion ubicacion;
    
}
