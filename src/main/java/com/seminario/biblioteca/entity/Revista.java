/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity(name = "Revista")
@Table(name = "revista")
@Data
public class Revista implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "nombreRevista", nullable = false)
    private String nombreRevista;
    
    @Column(name = "fechaRevista", nullable = false)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fecha;
    
    @Column(name = "numeroPaginas", nullable = false)
    private Integer numeroPaginas;
    
    @OneToMany(mappedBy = "revista")
    private List<Recurso> recursos;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ubicacion", referencedColumnName = "id", nullable = false)
    private Ubicacion ubicacion;
    
}
