/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity(name = "Recursos")
@Table(name = "recursos")
@Data
public class Recurso implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "codigo", nullable = false)
    private String codigo;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "libro", referencedColumnName = "id", nullable = false)
    private Libro libro;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "revista", referencedColumnName = "id", nullable = false)
    private Revista revista;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "proyectoGrado", referencedColumnName = "id", nullable = false)
    private ProyectoGrado proyectoGrado;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "estado", referencedColumnName = "id", nullable = false)
    private Estado estado;
}
