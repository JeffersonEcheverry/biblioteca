/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity(name = "Ubicacion")
@Table(name = "ubicacion")
@Data
public class Ubicacion implements Serializable {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "ubicacion")
    private String ubicacion;
    
    @OneToMany(mappedBy = "ubicacion")
    private List<Libro> libros;
    
    @OneToMany(mappedBy = "ubicacion")
    private List<ProyectoGrado> proyectoGrados;
    
    @OneToMany(mappedBy = "ubicacion")
    private List<Revista> revistas;
}
