/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jeffersonecheverry
 */
@Entity(name = "Persona")
@Table(name = "persona")
@Data
public class Persona implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    
    @Column(name = "documento", nullable = false, length = 20)
    private String documento;
    
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;
    
    @Column(name = "apellido", nullable = false, length = 50)
    private String apellido;
    
    @Column(name = "telefono", nullable = false, length = 10)
    private String telefono;
    
    @OneToOne(mappedBy = "persona")
    private Usuario usuario;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tipoUsuario", referencedColumnName = "id", nullable = false)
    private TipoUsuario tipoUsuario;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "authorities",
        joinColumns = {@JoinColumn(name = "persona", referencedColumnName = "id", nullable = false)},
        inverseJoinColumns = {@JoinColumn(name = "authority", referencedColumnName = "id", nullable = false)}
    )
    private Set<Authority> authorities;
}
