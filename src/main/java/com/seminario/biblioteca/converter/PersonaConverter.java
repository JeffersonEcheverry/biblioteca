/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.converter;

import com.seminario.biblioteca.entity.Persona;
import com.seminario.biblioteca.entity.Usuario;
import com.seminario.biblioteca.modelo.PersonaModelo;
import org.springframework.stereotype.Component;

/**
 *
 * @author jeffersonecheverry
 */
@Component("personaConverter")
public class PersonaConverter {
    
    public PersonaModelo converterPersonaToPersonaModelo(Persona persona, Usuario usuario){
        PersonaModelo personaModelo = new PersonaModelo();
        personaModelo.setNombre(persona.getNombre() != null ? persona.getNombre() : "");
        personaModelo.setApellido(persona.getApellido() != null ? persona.getApellido() : "");
        personaModelo.setDocumento(persona.getDocumento() != null ? persona.getDocumento() : "");
        personaModelo.setTelefono(persona.getTelefono() != null ? persona.getTelefono() : "");
        personaModelo.setTipoUsuario(persona.getTipoUsuario().getId() != null ? persona.getTipoUsuario().getId() : 0);
        personaModelo.setCodigo(usuario.getCodigo() != null ? usuario.getCodigo() : "");
        personaModelo.setPassword(usuario.getPassword() != null ? usuario.getPassword() : "");
        return personaModelo;
    }
    
}
