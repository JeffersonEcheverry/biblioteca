/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.service.implementation;

import com.seminario.biblioteca.converter.PersonaConverter;
import com.seminario.biblioteca.entity.Persona;
import com.seminario.biblioteca.entity.Usuario;
import com.seminario.biblioteca.modelo.PersonaModelo;
import com.seminario.biblioteca.respository.PersonaRepository;
import com.seminario.biblioteca.respository.UsuarioRepository;
import com.seminario.biblioteca.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeffersonecheverry
 */
@Service("personaService")
public class PersonaServiceImp implements PersonaService {
    
    @Autowired
    @Qualifier("personaRepository")
    private PersonaRepository personaRepository;

    @Autowired
    @Qualifier("usuarioRepository")
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    @Qualifier("personaConverter")
    private PersonaConverter personaConverter;
    
    @Value("${EXITO}")
    private int exito;
    
    @Override
    public PersonaModelo findPersona(String documento) throws Exception {
        Persona persona = personaRepository.findPersonaByDocumento(documento);
        if(persona != null){
            Usuario usuario = usuarioRepository.findUsuarioByPersona(persona.getId());
            return usuario != null ? personaConverter.converterPersonaToPersonaModelo(persona, usuario) : null;
        }
        return null;
    }

    @Override
    public PersonaModelo savePersona(PersonaModelo personaModelo) throws Exception {
        Persona persona = personaRepository.findPersonaByDocumento(personaModelo.getDocumento());
        if(persona == null){
            if(personaRepository.savePersona(personaModelo.getApellido(),
                    personaModelo.getDocumento(),
                    personaModelo.getNombre(),
                    personaModelo.getTelefono(),
                    personaModelo.getTipoUsuario()) == exito){
                return usuarioRepository.createUsuarioByPersona(personaModelo.getCodigo(), personaModelo.getPassword(), persona.getId()) == exito ?
                personaModelo : null;
            }
        }else{
            if(personaRepository.updatePersona(personaModelo.getNombre(),
                                               personaModelo.getTelefono(),
                                               personaModelo.getTipoUsuario(),
                                               personaModelo.getDocumento()) == exito){
                return usuarioRepository.updateUsuarioByPersona(personaModelo.getPassword(), persona.getId()) == exito ?
                        personaModelo : null;
            }
        }
        return null;
    }

    @Override
    public boolean deletePersona(String documento) throws Exception {
        Persona persona = personaRepository.findPersonaByDocumento(documento);
        if(persona != null){
            if(usuarioRepository.deleteUsuarioByPersona(persona.getId()) == exito){
                return personaRepository.deletePersona(documento) == exito;
            }
        }
        return false;
    }
    
}
