/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.service;

import com.seminario.biblioteca.modelo.PersonaModelo;

/**
 *
 * @author jeffersonecheverry
 */
public interface PersonaService {
    
    PersonaModelo findPersona(String documento) throws Exception;
    
    PersonaModelo savePersona(PersonaModelo personaModelo) throws Exception;

    boolean deletePersona(String documento) throws Exception;
    
}
