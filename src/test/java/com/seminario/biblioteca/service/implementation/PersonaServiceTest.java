/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seminario.biblioteca.service.implementation;

import com.seminario.biblioteca.entity.Persona;
import com.seminario.biblioteca.modelo.PersonaModelo;
import com.seminario.biblioteca.respository.PersonaRepository;
import com.seminario.biblioteca.service.PersonaService;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.junit.Assert.*;

/**
 *
 * @author jeffersonecheverry
 */
public class PersonaServiceTest {
    
    private PersonaService personaService;
    private Persona persona;
    private PersonaRepository personaRepository;
    
    @Before
    public void setup()throws Exception {
        System.out.println("Ejecuto el setup");
        personaRepository = Mockito.mock(PersonaRepository.class);
        persona = new Persona();
            persona.setNombre("Jefferson");
            persona.setApellido("Echeverry");
            persona.setDocumento("1133252112");
            persona.setTelefono("3002335566");
        Mockito.when(personaRepository.findPersonaByDocumento("1133252112")).thenReturn(persona);
    }
    
    @Test
    public void retornar_persona_por_documento(){
        System.out.println("Ejecuto la prueba");
        try{
            PersonaModelo persona = personaService.findPersona("1133252112");
            System.out.println("La persona es "+persona);
            assertThat(1, CoreMatchers.is(1));
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
